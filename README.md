# SatNOGS COMMS System Design Document #

SatNOGS COMMS System Design Document.

## Rendered documents ##

* [System Requirements](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/system-requirements.pdf?job=docs)
* [System Design](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/system-design.pdf?job=docs)
* [Quality Assurance Plan](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/quality-assurance-plan.pdf?job=docs)
* [Verification Plan](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/verification-plan.pdf?job=docs)
* [Design and Development Plan](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/design-development-plan.pdf?job=docs)
* [ICD](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/ICD.pdf?job=docs)
* [User Manual](https://cloud.libre.space/s/TBQ2KMwtzRFgt9R)
* [Thermal analysis](https://cloud.libre.space/s/XcBmHdaymREnan7)
* [Mechanical analysis] (https://cloud.libre.space/s/MixHABL7pFcyiXq)
