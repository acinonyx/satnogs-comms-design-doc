% SatNOGS COMMS Interface Control Document
% Copyright (C) 2020, 2021 Libre Space Foundation
%
% This work is licensed under a
% Creative Commons Attribution-ShareAlike 4.0 International License.
%
% You should have received a copy of the license along with this
% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

% Linter settings
% chktex-file -2

\documentclass[english,title,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage[toc,acronyms,section,nopostdot,nonumberlist,section=section,numberedsection]{glossaries}
\usepackage[toc,page]{appendix}
\usepackage{graphicx}
\usepackage{titlesec}
\usepackage{amssymb}
\usepackage{textcomp,gensymb}
\usepackage{booktabs}
\usepackage{pifont}
\usepackage{verbatim}
\usepackage{rotating}
\usepackage{makecell}
\usepackage{lipsum}
\usepackage{longtable}
\usepackage[paper=portrait,pagesize]{typearea}

\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\input{include/version}
\input{include/requirements}

\makeglossaries{}

%% Acronyms
\input{glossaries/acronyms}

\title{Interface Control Document \\\large{SatNOGS COMMS}}
\author{Libre Space Foundation}
\date{\today\\Version \version\\\textit{DRAFT}}

\titleformat{\chapter}[hang]
{\normalfont\bfseries\Huge}{\thechapter.}{10pt}{}

\begin{document}

\hypersetup{pageanchor=false}
\maketitle
\hypersetup{pageanchor=true}
\tableofcontents

\chapter{Introduction}

\section{Purpose}
This Interface Control Document (ICD) defines the interface requirements for integrating SatNOGS COMMS into a Cubesat.

\chapter{Mechanical interface}\label{sec:mechanical-interface}

%% SRR - MS-11, SRR - MS-10
\acrshort{pcb} dimensions, mounting holes, connector type and location follow LibreCube~\cite{LibreCube} standard.
Interface connectors are SAMTEC ESQ-126--39--G-D\@.
Mechanical drawing is shown in \autoref{fig:librecube-mechanical}

\begin{figure}[H]
  \centering
  \includegraphics[keepaspectratio, width=0.8\textwidth]{diagrams/librecube-board-outer-dimensions.png}
  \caption{LibreCube mechanical drawing (dimensions in mm)}\label{fig:librecube-mechanical}
\end{figure}

\chapter{Subsystem interface}\label{sec:subsystem-interface}

%% SRR - MS-22. Issue #6
\section{Subsystem Communication}\label{sec:subsystem-communication}
For subsystem communication the following interfaces are available:
\begin{itemize}
\item Two \acrshort{can}-FD interfaces backward compatible with \acrshort{can}-2.0
\item \acrshort{spi} Interface having maximum link speed of 8Mbps
\end{itemize}
\acrshort{can}-FD fulfills interconnect link speed requirements.
Subsystems only capable of \acrshort{can}-2.0 can use \acrshort{spi} for data transfer and \acrshort{can}-2.0 for control.

\acrshort{rf} connectors will be either SMA or MCX

\section{Antenna deployment}\label{sec:antenna-deployment}

%% SRR - MS-09
Antenna deployment interface exposes two closed loop control endpoints.
Each endpoint consists of control signal and detection signal lines exposed via a Hirose DF11--8DP-2DS (52) with option of Hirose DF11--8DP-2DSA (01).
Additionally these lines can be available on the main connector.

Control signal lines are software configurable as either 3.3V Push-Pull or Open-Drain.
Maximum input voltage in Open-Drain configuration is restricted to 5V by protection diode.
Control signal current is limited to 18mA via $270\Omega$ resistor

Detection signal lines are 3.3V compatible and software configured as Pull-Up, Pull-Down or Floating.
Pull-Up and Pull-Down resistor value is $40k\Omega \pm 10k\Omega$.
Each detection line is protected from over-voltage via 3.3V Zener diode and $330\Omega$ resistor.

Electrical connection of signal lines is provided via an 8 pin connector as shown in \autoref{fig:ant-deploy}
\section{Power supply}\label{sec:power-supply}

COMMS transceiver accepts an input voltage range varying from 6V DC to 14V DC\@.
Absolute maximum power consumption is estimated at 20W.
This value should never be reached under normal operating conditions.

\section{Ground Segment}\label{sec:ground-segment}

\subsection{Telemetry API}\label{sec:telemetry-api}

SatNOGS provides an \acrshort{api} for receiving acquired telemetry as well as signal acquisition parameters.
For non real-time operation, raw telemetry data is uploaded directly to SatNOGS DB servers by SatNOGS Client, on predefined intervals.
This \acrshort{api} endpoint accepts \acrshort{hdf5} formatted files which include additional information and metadata of the scheduled SatNOGS Network observation.
The telemetry data are then processed and forward to the InfluxDB database of SatNOGS Dashboards in order to be visualized.
For real-time operation, SatNOGS Client forwards the telemetry data to a SatNOGS Yamcs server.
The SatNOGS Yamcs server receives the data through a data link plugin which provides a \acrshort{grpc} interface for telemetry.
In this case, telemetry data is still eventually forwarded to SatNOGS DB via the Yamcs server and a SatNOGS DB \acrshort{api} data link plugin.

\subsection{Telecommand API}\label{sec:telecommand-api}

SatNOGS provides an \acrshort{api} allowing telecommand data transmission.
SatNOGS Client \acrshort{grpc} interface is used to receive telecommands.
These commands are sent by the SatNOGS Yamcs server via a \acrshort{grpc} data link plugin.
The client then forwards the telecommands to SatNOGS Radio in order to be encoded and transmitted via the \acrshort{sdr}.

\chapter{Connector Interface}

\section{BUS Connector}\label{sec:bus-connector}

Connector type: SAMTEC ESQ-126–39–G-D\newline
Pin assignment: Based on Librecube standard~\cite{LibreCube} \autoref{tab:bus-pinout}

\begin{table}[H]
  \centering
  \begin{tabular}{|l|l|l|}
    \hline
    Pin & J102             & J103      \\ \hline
    1   & CAN A Low        & CAN B Low \\ \hline
    3   & CAN A Low        & CAN B Low \\ \hline
    11  & SPI MISO         & ---       \\ \hline
    13  & SPI MOSI         & ---       \\ \hline
    15  & SPI CLK          & ---       \\ \hline
    17  & Antenna Detect 1 & ---       \\ \hline
    18  & Antenna Deploy 1 & ---       \\ \hline
    19  & Antenna Detect 2 & ---       \\ \hline
    20  & Antenna Deploy 2 & ---       \\ \hline
    21  & I2C SCL          & ---       \\ \hline
    23  & I2C SDA          & ---       \\ \hline
    39  & VIN              & ---       \\ \hline
    40  & VIN              & ---       \\ \hline
  \end{tabular}%

  \caption{Programming connector pin assignment}\label{tab:bus-pinout}
\end{table}

\section{Antenna Deploy Connector}\label{sec:antenna-connector}
Connector type: Hirose DF11--6DP-2DS\newline
Pin assignment: \autoref{tab:ant-deploy-pinout}

\begin{table}[H]
  \centering
  \begin{tabular}{|l|l|}
    \hline
    Pin & Function \\ \hline
    1   & Deploy-1 \\ \hline
    2   & Detect-1 \\ \hline
    3   & Deploy-2 \\ \hline
    4   & Detect-2 \\ \hline
    5   & GND      \\ \hline
    6   & GND      \\ \hline
  \end{tabular}%

  \caption{Antenna Deploy connector pin assignment}\label{tab:ant-deploy-pinout}
\end{table}

\section{Programming connector}\label{sec:programming-sonnector}
Connector type: Hirose DF11--8DP-2DSA \newline
Supported interfaces: SWD and JTAG\newline
Pin assignment: \autoref{tab:programming-pinout}
\begin{table}[H]
  \centering
  \begin{tabular}{|l|l|l|}
    \hline
    Pin & SWD            & JTAG           \\ \hline
    1   & 3.3V Reference & 3.3V Reference \\ \hline
    2   & Clock          & Clock          \\ \hline
    3   & GND            & GND            \\ \hline
    4   & IO             & JTMS           \\ \hline
    5   & RESET          & RESET          \\ \hline
    6   & SWO            & JTDO           \\ \hline
    7   & ---            & JTDI           \\ \hline
    8   & ---            & ---            \\ \hline
  \end{tabular}%

  \caption{Programming connector pin assignment}\label{tab:programming-pinout}
\end{table}

\section{RF Connectors}\label{sec:rf-connectors}
Antenna connectors can be SMA or MCX, straight or angled depending on user requirements
\chapter{Interfaces Data Sheet}

%% Power
\begin{table}[ht]
  \centering
  \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|p{0.35\linewidth}|p{0.6\linewidth}|l|l|}
      \hline
      \multicolumn{5}{|c|}{\textbf{INTERFACE DATA SHEET}} \\ \hline
      & I/F Designation: & \textbf{DC Power interface} & \multicolumn{2}{l|}{} \\ \hline
      \textbf{ID} & \multicolumn{2}{l|}{\textbf{Source Circuit Specification}} & Ver & Iss \\ \hline
      -1 & Bus voltage & 6V-14V input range &  & \\ \hline
      -2 & Bus current & 1.6A max @ 14V Bus voltage \newline 3.5A max @ 6V Bus voltage &  & \\ \hline
      -3 & Response to bus undervoltage & All loads except MCU switch off automatically MCU is deactivated at 3.2V Bus voltage & & \\ \hline
      -4 & Bus voltage (anomaly): & The load shall not be damaged when subjected to any bus voltage in the range 0V to 14V, steady-state or at any rate of change. & & \\ \hline
    \end{tabular}%
  }
  \caption{DC Power Interface characteristics}\label{tab:dc-if-power}

\end{table}

%% SPI
\begin{table}[ht]
  \centering
  \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|p{0.35\linewidth}|p{0.6\linewidth}|l|l|}
      \hline
      \multicolumn{5}{|c|}{\textbf{INTERFACE DATA SHEET}} \\ \hline
      & I/F Designation: & \textbf{SPI interface} & \multicolumn{2}{l|}{} \\ \hline
      \textbf{ID} & \multicolumn{2}{l|}{\textbf{Voltage, data rate and timing specification}} & Ver & Iss \\ \hline
      -1 & Data rate & 8 Mbps for 3m total bus length between devices &  & \\ \hline
      -2 & Voltage range & 0V--3.3V &  & \\ \hline
      -3 & Voltage tolerance & -0.3V--5.5V &  & \\ \hline
      -4 & SPI clock frequency & Slave receiver mode: 100MHz max \newline Slave mode transmitter/full duplex: 31MHz \newline Slave mode transmitter/full duplex: 25MHz & & \\ \hline
      -5 & NSS setup time & Min 2ns &  & \\ \hline
      -6 & NSS hold time & Min 1ns &  & \\ \hline
      -7 & Data input setup time & Min 2ns &  & \\ \hline
      -8 & Data input hold time & Min 1ns &  & \\ \hline
      -9 & Data input hold time & 1ns &  & \\ \hline
      -10 & Data output access time & 9ns--27ns &  & \\ \hline
      -11 & Data output disable time & Max 5ns &  & \\ \hline
      -12 & Data output valid time & Max 16ns &  & \\ \hline
      -13 & Data output hold time & Min 9ns &  & \\ \hline
    \end{tabular}%
  }
  \caption{SPI Interface characteristics}\label{tab:if-spi}
\end{table}

%% CAN Bus
\begin{table}[ht]
  \centering
  \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|p{0.35\linewidth}|p{0.6\linewidth}|l|l|}
      \hline
      \multicolumn{5}{|c|}{\textbf{INTERFACE DATA SHEET}} \\ \hline
      & I/F Designation: & \textbf{CAN interface} & \multicolumn{2}{l|}{} \\ \hline
      \textbf{ID} & \multicolumn{2}{l|}{\textbf{CAN Bus characteristics}} & Ver & Iss \\ \hline
      -1 & Bus 1 & compliant with ISO 11898--1\cite{ISO-11898-1} (CAN protocol specification version 2.0 part A, B) and CAN FD protocol specification version 1.0 \newline Supports time triggered CAN (TT-FDCAN) specified in ISO 11898--4\cite{ISO-11898-4} &  & \\ \hline
      -2 & Bus 2 & compliant with ISO 11898--1\cite{ISO-11898-1} (CAN protocol specification version 2.0 part A, B) and CAN FD protocol specification version 1.0 &  & \\ \hline
      -3 & Receiver common mode input voltage & +/-12V &  & \\ \hline
      -4 & Bus fault protection & +/-58V &  & \\ \hline
      -5 & Max differential voltage between CANH and CANL & +/-45V &  & \\ \hline
      -6 & Bus termination & 120 $\Omega$ at each end of the bus. Split termination is supported &  & \\ \hline
    \end{tabular}%
  }
  \caption{CAN Interface characteristics}\label{tab:if-can}
\end{table}

%% Antenna
\begin{table}[ht]
  \centering
  \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|p{0.35\linewidth}|p{0.6\linewidth}|l|l|}
      \hline
      \multicolumn{5}{|c|}{\textbf{INTERFACE DATA SHEET}} \\ \hline
      & I/F Designation: & \textbf{Antenna interface} & \multicolumn{2}{l|}{} \\ \hline
      \textbf{ID} & \multicolumn{2}{l|}{\textbf{RF characteristics}} & Ver & Iss \\ \hline
      -1 & UHF Impedance & 50 $\Omega$ &  & \\ \hline
      -2 & S-Band impedance & 50 $\Omega$ &  & \\ \hline
      \multicolumn{5}{|l|}{\textbf{Deployment control}} \\ \hline
      -3 & Output type & Push-pull or Open-drain &  & \\ \hline
      -4 & Output voltage & Push-pull: 3.3V \newline Open-drain: 5V &  & \\ \hline
      -5 & Output current & Max 18mA &  & \\ \hline
      \multicolumn{5}{|l|}{\textbf{Deployment detection}} \\ \hline
      -6 & Input type & Pull-Up, Pull-Down or Floating &  & \\ \hline
      -7 & Input voltage & Nominal 3.3V \newline Tolerant 5V &  & \\ \hline
      -8 & High threshold & 2V &  & \\ \hline
      -9 & Low threshold & 0.8V &  & \\ \hline
      -10 & Pull-up/Pull-down resistor & 40k$\Omega$±10k$\Omega$ &  & \\ \hline
    \end{tabular}%
  }
  \caption{Antenna Interface characteristics}\label{tab:if-antenna}
\end{table}

\bibliographystyle{unsrt}
\bibliography{references}
\end{document}