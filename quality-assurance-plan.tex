% SatNOGS COMMS System Design Document
% Copyright (C) 2020, 2021 Libre Space Foundation
%
% This work is licensed under a
% Creative Commons Attribution-ShareAlike 4.0 International License.
%
% You should have received a copy of the license along with this
% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

% Linter settings
% chktex-file -2

\documentclass[english,title,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage[toc,acronyms,section,nopostdot,nonumberlist,section=section,numberedsection]{glossaries}
\usepackage[toc,page]{appendix}
\usepackage{graphicx}
\usepackage{titlesec}
\usepackage{amssymb}
\usepackage{textcomp,gensymb}
\usepackage{booktabs}
\usepackage{pifont}
\usepackage{verbatim}
\usepackage{rotating}
\usepackage{makecell}
\usepackage{lipsum}
\usepackage{longtable}
\usepackage{rotating}
\usepackage{multirow}
\usepackage{amssymb}
\usepackage{xurl}

\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\input{include/version}
\input{include/requirements}

\makeglossaries{}

%% Acronyms
\input{glossaries/acronyms}

\title{Quality Assurance Plan \\\large{SatNOGS COMMS}}
\author{Libre Space Foundation}
\date{\today\\Version \version\\\textit{DRAFT}}

\titleformat{\chapter}[hang]
{\normalfont\bfseries\Huge}{\thechapter.}{10pt}{}

\begin{document}

\hypersetup{pageanchor=false}
\maketitle
\hypersetup{pageanchor=true}
\tableofcontents

\chapter{Introduction}

\section{Purpose and Scope}

This \acrshort{qa} plan focus mainly on the hardware components and their assembly on the \acrshort{pcb} of the SatNOGS COMMS transceiver.
Regarding the software components, throughout the entire development all the affected software packages integrate \acrshort{qa} techniques using automated tests and coverage reports.
The \acrshort{qa} sessions of the assembled \acrshort{pcb}, is performed by the designers of the board, using semi-automated testing procedures, ensuring the end to end functionality and interoperability based on in house test-bed platform~\cite{sdr-testbed}.

On par, a non-conformance control system ensures that possible deviations from the initial requirements are identified early and possible mitigation strategies are explored.
\printglossary[type=\acronymtype]

\chapter{Quality Assurance Plan}

\section{Procurement}

The selection of procurement sources for \acrshort{eee} is based on the ECSS-Q-ST-60C clause 5.2.2, except from clause 5.2.2.3 which refers to in radiation hardness.
The SatNOGS COMMS transceiver targets the Cubesat market.
In this spectrum, radiation hardened components are not necessary and in most cases
are prohibitive in terms of cost for the limited budget of such missions.
In addition, the procurement strategies ensure that the components selected do not reach their \acrshort{eol} and will be available for long time.
The procurement of the \acrshortpl{pcb} is full-filled by sources conforming with the IPC-A-610E Class 2 directives.

For all the components from external suppliers, a detailed \acrshort{bom} document is tracking the components and the source suppliers, while at the same time it records for possible defects or deviation from nominal specification values.

\section{Manufacturing, assembly and integration}

Manufacturing of the \acrshortpl{pcb} and \acrshort{eee} components is performed by external suppliers that conform with the IPC-A-610 Class 3 specification.
In order to ensure the product quality, hardware components storage and handling follows the ECSS-Q-ST-20C, clause 5.2.7.

The assembly and the integration takes place in the \acrshort{lsf} facilities.
The process starts with the unpacking of the externally supplied components.
To minimize cross-contamination, before unpacking the components their packaging is cleared using dry air.
Large components, like \acrshortpl{pcb} and heatsinks are wiped and cleaned with \acrshort{ipa} and deionized water pre-wetted low particle wipers.
Then they are stored in sealed antistatic \acrshort{esd}-safe bags until their final usage.
Supplied \acrshort{eee} components are considered to be contaminant-free in their \acrshort{esd}-safe packaging.

Assembly of the SatNOGS COMMS board is performed in a controlled environment at a dedicated for this purpose area, taking measures to minimize contamination and \acrshort{esd}.
Before any assembly, the soldering mask stencil, the stencil printer, the pick-and-place and similar tooling is also cleaned with dry air and wipers to remove any kind of debris.
After the \acrshort{eee} components placement, the \acrshort{pcb} process continues with the soldering in the reflow oven.
With the reflow process completed, the assembled \acrshort{pcb} undergoes extensive cleaning with dry air, \acrshort{ipa} and wipers to remove any left over contaminants.
Then it is placed inside an ISO 7 cleanbox available at \acrshort{lsf} premises.
From this point and until the final delivery, the SatNOGS COMMS board does not exit the cleanbox, until the final packaging.

After the assembly the integration process follows.
This process includes the conformal coating of the \acrshort{pcb} and visual inspection using \acrshort{uv} light to ensure proper application of the coating.
The integration process finalizes with the placement of the heatsink blocks.

\section{Testing}

Testing and performance measurements are full-filled by \acrshort{lsf} at its own facilities.
The hardware equipment used for testing (spectrum analyzers, DC supplies, vector analyzers) is ensured to have a valid calibration certificate.

Each \acrshort{dut}, is subjected to predefined and automated test according the the test plan.
The complete verification and test plan can be found in the \textbf{Verification and Test Plan} document.
Each testing stage produces reports that include the observed performance values and their deviation from the nominal and expected performance.
If all measurements are within the acceptance margins, a final report is produced, including the date and the personnel conducted the tests, among with other informative details, like equipment used, etc.

In case the \acrshort{dut} fails to conform with the tests, it is discarded.
No fixing or disassembly attempts are permitted at this stage.
This event is triggering an internal investigation for tracing the defected parts or deviations from the manufacturing procedure.

\section{Acceptance and delivery}

Before any delivery, a final review of the testing results is performed by the \acrshort{qa} personnel.
Each delivered item is accompanied by the report of the testing campaign, the date of manufacturing, the serial number of the board and the name of the engineer conducted and approved the tests.

Prior shipping a proper packaging ensures the minimum possible risk of damage.
The SatNOGS COMMS board is enclosed in a heat sealed \acrshort{esd} safe bag.
At this point the board can exit the cleanbox and is stored in a dedicated place of the laboratory.

Prior delivery, the board is firmly placed inside single-wall corrugated chipboard using Polyethylene Foam A-A-59136, Class 1, Grade A, Type 1.
Proper labeling is applied to the packaging, with ESD sensitive and fragile markings.

\section{Declared Components List}

All the component of SatNOGS COMMS Transceiver neither designed nor manufactured with reference to military or space standards.
The \acrshort{eee} shall be \acrshort{cots} which not included in parts and material restriction as defined in ECSS-Q-ST-60C~\cite{ECSS-Q-ST-60C}, clause 5.2.2.2.
Also all the \acrshort{eee} shall be recommended for new designs or could be active from component manufacturer.
For all the \acrshort{eee} should be provided compliance with tailored standards for In-Orbit
Demonstration Cubesat missions and flight heritage~\cite{ECSS-Cubesat}.
It mentions that radiation testing is not applicable according to tailored standards for In-Orbit Demonstration Cubesat missions.
For that reason in \acrshort{dcl}, does not include all proposed field as is defined in the ECSS-Q-ST-60C~\cite{ECSS-Q-ST-60C} Annex B.
The fields of list are:
\begin{itemize}
\item Footprint: Contains the footprint type, for example SMD 0603.
\item Mfr Name: Contains the name of manufacturer.
\item Mfr Part Number: Contains the part number of manufacturer.
\item Description: Describe the type of part, for example Capacitor, 2.2uF-16V-X5R-10\%
\item Mfr Name-–1: The first alternative manufacturer.
\item Mfr Part Number-–1: The first alternative manufacturer part number.
\item Mfr Name–-2: The second alternative manufacturer.
\item Mfr Part Number-–2: The second alternative manufacturer part number.
\end{itemize}
The \acrshort{dcl} will be generated from \acrshort{eda} software, KiCad~\cite{kicad} and will be available in \acrshort{csv} format.

\section{Declared Materials List}

All the components shall be selected according to ECSS-Q-ST-70--12C~\cite{ECSS-Q-ST-70-12C}.
More specific the \acrshort{pcb} material shall be follow the ECSS-Q-ST-70--12C~\cite{ECSS-Q-ST-70-12C}, clause 12.
For all the components should be provided compliance with tailored standards for In-Orbit
Demonstration Cubesat missions and flight heritage.
In order to improve the thermal management of the system, possibly a heatsink could be used.
The material selection shall be follow the ECSS-Q-ST-70--12C~\cite{ECSS-Q-ST-70-12C}, clause 10.
Additional materials which would be arise like from the design of heatsink bonding in to \acrshort{pcb} shall be added in the \acrshort{dml}.
The \acrshort{dml} shall be populated according to ECSS‐Q‐ST‐70C~\cite{ECSS-Q-ST-70C}, Annex A.
The fields of list are:
\begin{itemize}
\item Commercial identification or standardized designation.
\item Chemical nature and product type.
\item Use and location.
\end{itemize}

According to ECSS‐Q‐ST‐70C~\cite{ECSS-Q-ST-70C} Rev.1 Annex A, the \acrshort{dmpl} is part of \acrshort{dml} as a separate group with the corresponding numbers.

\section{Declared Process List}

\acrshort{dpl} is populated according ECSS‐Q‐ST‐70C~\cite{ECSS-Q-ST-70C} Annex C.
As described in section Manufacturing, assembly and integration of Quality Assurance Plan the assembly of PCB (soldering, cleaning, etc) will be done by external supplier.
This is a process that will be included in the DPL\@.
The machining and coating of heatsink parts will be included in \acrshort{dpl}.
Integration processes, like the assembly of assembled PCB with heatsink parts will be included in this list.

\section{Failure Modes and Effects Analysis}

The complete FMEA can be found in \textbf{Failure Modes and Effects Analysis} chapter of the \textbf{System Design Document}.

\chapter{Non-conformance Management Plan}

\section{Process and objectives}

Throughout the development of the SatNOGS COMMS system a non-conformance control system ensures that any kind of \acrshort{nc} are quickly identified and the affected stakeholders are informed.
The routines of the control system are presented in \autoref{fig:ncr}.
This control system is derived from ECSS-Q-ST-10--09C~\cite{ECSS-Q-ST-10-09C} and adapted to the workflow of \acrshort{lsf}.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{diagrams/ncr.pdf}
  \caption{Non-conformance processing flow chart}\label{fig:ncr}
\end{figure}

\section{Non-conformance detection}

A \acrshort{nc} can be detected at any phase of the development,testing or \acrshort{qa} phase.
When \acrshort{nc} item is detected, an internal \acrshort{ncr} is created.
The \acrshort{ncr} has the form of a Gitlab issue at the issue board of the organizational repository of the SatNOGS COMMS system (\url{https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-org/-/boards}).
The \acrshort{ncr}, lists all the \acrshort{nc} requirements and is immediately assigned to an engineer, responsible to introduce this \acrshort{nc} to the internal \acrshort{nrb}.

A \acrshort{nc} detection, triggers an engineering meeting.
The purpose of this meeting is to analyze the root cause of the \acrshort{nc},
identify the affected items and discuss possible solutions or workarounds that could mitigate the \acrshort{nc}.

\section{Non-conformance review board}

\subsection{Internal NRB}

The internal \acrshort{nrb} investigates the root causes of the \acrshort{nc} and lists all the affected components of the project.
It is also responsible to block all the affected ongoing tasks, utilizing the corresponding feature of the Gitlab issue tracker, until the close-out of the \acrshort{ncr}.
Then, the internal \acrshort{nrb} classifies the \acrshort{nc}, as minor or major.
In case the \acrshort{nc} is minor, an \acrshort{rfd} is compiled and the Agency is informed.
If the \acrshort{rfd} is accepted, the process continues with the necessary corrective and preventive actions.

On the event of a major \acrshort{nc} or a rejected \acrshort{rfd}, a \acrshort{nrb} meeting with the agency is arranged for further discussion.

In addition, the internal \acrshort{nrb} may review feedback originating from the Agency \acrshort{nrb} and evaluate the need of a \acrshort{rfw}.

\subsection{Agency NRB}

The Agency \acrshort{nrb} follows the same process with the internal one and feedback on the affected \acrshort{nc} is requested (if possible).

\section{Actions}

After the \acrshort{nrb} and possible feedback from the Agency, the affected \acrshort{qa} items are redefined accordingly.
Engineers implement all the necessary corrective and preventive actions and upon close-out all the affected blocked tasks at the Gitlab issue tracker are unblocked.
Possible \acrshort{qa} actions that failed due to the \acrshort{nc}, are revised and re-triggered.

\chapter{Safety-Hazard Requirements}

\section{Generic Hazards}

\begin{table}[H]
  \centering
  \resizebox{0.88\textwidth}{!}{%
    \begin{tabular}{|c|l|l|}
      \hline
      \multicolumn{3}{|c|}{\textbf{Hazard matrix of SatNOGS COMMS transceiver}} \\ \hline
      \multicolumn{2}{|c|}{\textbf{Generic Hazards}} &  \\ \hline
      \multirow{5}{*}{
      \begin{tabular}[c]{@{}c@{}}Thermodynamic\\ and\\ fluidic\end{tabular}}
                                                     & Pressure  &  --- \\ \cline{2-3}
                                                     & Temperature  & \checkmark{} \\ \cline{2-3}
                                                     & Heat transfer &  \checkmark{} \\ \cline{2-3}
                                                     & Fluid Jet &  --- \\ \cline{2-3}
                                                     & \begin{tabular}[c]{@{}l@{}}Thermal properties of materials\end{tabular} & --- \\ \hline

      \multirow{6}{*}{
      \begin{tabular}[c]{@{}c@{}}Electrical and electromagnetic \end{tabular}}
                                                     & Voltage  &  \checkmark{} \\ \cline{2-3}
                                                     & Static electricity  & \checkmark{} \\ \cline{2-3}
                                                     & Electric current &  \checkmark{} \\ \cline{2-3}
                                                     & Magnetic field &  --- \\ \cline{2-3}
                                                     & Ionization &  --- \\ \cline{2-3}
                                                     & \begin{tabular}[c]{@{}l@{}}Sparks\end{tabular} & --- \\ \hline

      \multirow{3}{*}{
      \begin{tabular}[c]{@{}c@{}}Radiation \end{tabular}}
                                                     & Light  &  --- \\ \cline{2-3}
                                                     & Radioactivity  & --- \\ \cline{2-3}
                                                     & \begin{tabular}[c]{@{}l@{}}Open fire\end{tabular} & \checkmark{} \\ \hline

      \multirow{6}{*}{
      \begin{tabular}[c]{@{}c@{}}Chemical \end{tabular}}
                                                     & Toxicity  &  --- \\ \cline{2-3}
                                                     & Corrosiveness  & --- \\ \cline{2-3}
                                                     & Flammability &  --- \\ \cline{2-3}
                                                     & Explosiveness &  --- \\ \cline{2-3}
                                                     & Asphyxiant &  --- \\ \cline{2-3}
                                                     & \begin{tabular}[c]{@{}l@{}}Irritant\end{tabular} & --- \\ \hline

      \multirow{3}{*}{
      \begin{tabular}[c]{@{}c@{}}Mechanical \end{tabular}}
                                                     & Physical impact or mechanical energy  &  --- \\ \cline{2-3}
                                                     & Mechanical properties of material  & --- \\ \cline{2-3}
                                                     & \begin{tabular}[c]{@{}l@{}}Vibration\end{tabular} & \checkmark{} \\ \hline

      Noise & Frequency and intensity & --- \\ \hline

      \multirow{3}{*}{
      \begin{tabular}[c]{@{}c@{}}Biological \end{tabular}}
                                                     & Human waste  &  --- \\ \cline{2-3}
                                                     & Micro-organism  & --- \\ \cline{2-3}
                                                     & \begin{tabular}[c]{@{}l@{}}Carcinogenic\end{tabular} & --- \\ \hline

      Psychological &  & --- \\ \hline

      Physical & Confined space & --- \\ \hline

      \multirow{7}{*}{
      \begin{tabular}[c]{@{}c@{}}Environment --- space \end{tabular}}
                                                     & Zero gravity  &  --- \\ \cline{2-3}
                                                     & Vacuum  & --- \\ \cline{2-3}
                                                     & Atmospheric composition &  --- \\ \cline{2-3}
                                                     & Contaminants, pollutants &  --- \\ \cline{2-3}
                                                     & Temperature &  \checkmark{} \\ \cline{2-3}
                                                     & Radiation &  \checkmark{} \\ \cline{2-3}
                                                     & \begin{tabular}[c]{@{}l@{}}South Atlantic anomaly\end{tabular} & \checkmark{} \\ \hline

      \multirow{3}{*}{
      \begin{tabular}[c]{@{}c@{}}Environment --- Earth \end{tabular}}
                                                     & Environmental extremes  &  --- \\ \cline{2-3}
                                                     & Natural disasters  & --- \\ \cline{2-3}
                                                     & \begin{tabular}[c]{@{}l@{}}Lightning\end{tabular} & --- \\ \hline

      \multicolumn{3}{|c|}{\checkmark{} = applicable~~~~~~~~~~~~~~~~--‐ = not applicable}  \\ \hline
    \end{tabular}
  }
\end{table}



\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|}
    \hline
    \multicolumn{2}{|c|}{\textbf{SatNOGS COMMS transceiver hazard manifestation list}} \\ \hline
    \textbf{Mission phase} & \textbf{Manifestation list} \\ \hline
    \multirow{1}{*}{\textbf{Ground operation}}
                           &  \begin{tabular}[c]{@{}l@{}}Handling and integration \\ of the SatNOGS COMMS transceiver \\ in an \acrshort{esd}-safe environment \end{tabular} \\ \hline
    \multirow{4}{*}{\textbf{In-orbit operation}}
                           &  \begin{tabular}[c]{@{}l@{}}Vibration forces during launch or deployment \\ inside the limits specified by~\cite{ECSS-E-ST-10-03C}\end{tabular} \\ \cline{2-2}
                           &   \begin{tabular}[c]{@{}l@{}}Power supply voltage exceeding \\ the maximum rating of 14V and \\
                                 unregulated current can cause fire\end{tabular} \\ \cline{2-2}
                           &   \begin{tabular}[c]{@{}l@{}}High energy particles can cause \\ system reboots or permanent damage\end{tabular} \\ \cline{2-2}
                           &   \begin{tabular}[c]{@{}l@{}}Temperature exceeding the operational limits \\ can cause reboots or permanent damage\end{tabular} \\ \hline

  \end{tabular}
\end{table}

The hazard scenario and consequence severity terminology and allocation is conforming with definitions as per ECSS-Q-30--02A~\cite{ECSS-Q-30-02A}.

\begin{table}[H]
  \centering
  \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|c|c|c|c|}
      \hline
      \multicolumn{5}{|c|}{\textbf{Hazard scenario list for ground operations phase}}  \\
      \hline
      \begin{tabular}[c]{@{}c@{}}\textbf{Hazard}\\\textbf{Manifestation}\end{tabular}
      & \begin{tabular}[c]{@{}c@{}}\textbf{Cause-}\\\textbf{Events-}\\\textbf{Consequence}\end{tabular}
      & \begin{tabular}[c]{@{}c@{}}\textbf{Consequence}\\\textbf{Severity}\end{tabular}
      & \begin{tabular}[c]{@{}c@{}}\textbf{Observable}\\\textbf{Symptoms}\end{tabular}
      & \begin{tabular}[c]{@{}c@{}}\textbf{Mitigation}\end{tabular}  \\
      \hline
      \begin{tabular}[c]{@{}c@{}}Static \\ electricity \end{tabular}
      & \begin{tabular}[c]{@{}c@{}} Assembly\\ and integration \\ in without \acrshort{esd} \\protection-\\ \acrshort{esd} event-\\failed \acrshort{eee}\end{tabular}
      & Critical 2
      & \begin{tabular}[c]{@{}c@{}}Loss of \\ communication,\\ abnormal \\operation \end{tabular}
      & \begin{tabular}[c]{@{}c@{}} Handling \\ in \acrshort{esd}-safe\\environment\end{tabular} \\
      \hline
    \end{tabular}
  }
\end{table}

\begin{table}[H]
  \centering
  \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|c|c|c|c|}
      \hline
      \multicolumn{5}{|c|}{\textbf{Hazard scenario list for in-orbit phase}}  \\
      \hline
      \begin{tabular}[c]{@{}c@{}}\textbf{Hazard}\\\textbf{Manifestation}\end{tabular}
      & \begin{tabular}[c]{@{}c@{}}\textbf{Cause-}\\\textbf{Events-}\\\textbf{Consequence}\end{tabular}
      & \begin{tabular}[c]{@{}c@{}}\textbf{Consequence}\\\textbf{Severity}\end{tabular}
      & \begin{tabular}[c]{@{}c@{}}\textbf{Observable}\\\textbf{Symptoms}\end{tabular}
      & \begin{tabular}[c]{@{}c@{}}\textbf{Mitigation}\end{tabular}  \\
      \hline
      \begin{tabular}[c]{@{}c@{}}Vibration \\ environment \end{tabular}
      & \begin{tabular}[c]{@{}c@{}} Launch vehicle \\ vibrations-\\ failed \acrshort{eee}-\\ loss of the transceiver \end{tabular}
      & Critical 2S
      & \begin{tabular}[c]{@{}c@{}}Loss of \\ communication,\\ abnormal \\operation \end{tabular}
      & \begin{tabular}[c]{@{}c@{}} Vibration tests, \\ soldering of \\ \acrshort{eee} following \\ IPC-A-610 Class 3\\ specification\end{tabular} \\
      \hline
      \begin{tabular}[c]{@{}c@{}}Voltage \\ and \\ electric \\ current\end{tabular}
      & \begin{tabular}[c]{@{}c@{}}Power supply \\ above 14V, \\unregulated current --- \\ short circuit \\can cause fire -\\ loss of spacecraft \end{tabular}
      & Critical 2S
      & None
      & \begin{tabular}[c]{@{}c@{}} Transceiver \\ power supply\\ must be off \\ until deployment,\\ hardware overvoltage\\supervisor\end{tabular} \\
      \hline
      \begin{tabular}[c]{@{}c@{}}Radiation\\environment\end{tabular}
      & \begin{tabular}[c]{@{}c@{}}\acrshort{seu} from \\ high energy particles-\\ memory corruption-\\ loss of spacecraft \end{tabular}
      & Critical 2
      & \begin{tabular}[c]{@{}c@{}}Loss of \\ communication,\\ abnormal \\operation,\\system reboots,\\ watchdog reboots \end{tabular}
      & \begin{tabular}[c]{@{}c@{}} Use \acrshort{ecc} \\ capable memories,\\use software \\techniques\\for \acrshort{ecc} and/or \acrshort{tmr},\\periodic update of \\ \acrshortpl{ic} registers  \end{tabular} \\
      \hline
    \end{tabular}
  }
\end{table}

\bibliographystyle{unsrt}
\bibliography{references}

\end{document}
