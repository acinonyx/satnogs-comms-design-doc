% SatNOGS COMMS System Design Document - Design and Development plan
% Copyright (C) 2020 Libre Space Foundation
%
% This work is licensed under a
% Creative Commons Attribution-ShareAlike 4.0 International License.
%
% You should have received a copy of the license along with this
% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

% Linter settings
% chktex-file -2

\documentclass[english,title,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage[toc,acronyms,section,nopostdot,nonumberlist,section=section,numberedsection]{glossaries}
\usepackage[toc,page]{appendix}
\usepackage{graphicx}
\usepackage{titlesec}
\usepackage{amssymb}
\usepackage{textcomp,gensymb}
\usepackage{booktabs}
\usepackage{pifont}
\usepackage{verbatim}
\usepackage{rotating}
\usepackage{makecell}
\usepackage{lipsum}
\usepackage{longtable}
\usepackage{rotating}

\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\input{include/version}
\input{include/requirements}

\makeglossaries{}

%% Acronyms
\input{glossaries/acronyms}

\title{Design and Development Plan \\\large{SatNOGS COMMS}}
\author{Libre Space Foundation}
\date{\today\\Version \version\\\textit{DRAFT}}

\titleformat{\chapter}[hang]
{\normalfont\bfseries\Huge}{\thechapter.}{10pt}{}

\begin{document}

\hypersetup{pageanchor=false}
\maketitle
\hypersetup{pageanchor=true}
\tableofcontents

\chapter{Introduction}

\section{Purpose and Scope}

This document describes design and development stages, activities and tasks of `SatNOGS-COMMS'.
It also include an update risk assessment of the design and development activities.

\printglossary[type=\acronymtype]

\chapter{Design and Development}

The development process is conducted in two parallel workgroups, one working on the space and the other working on the ground segment.
Although there are no blocking dependencies between the groups, at least in early development phases, the working groups are in close collaboration through frequent common meetings and progress reports to ensure interoperability of the two segments and the testing campaign.

\section{COMMS transceiver}

For the COMMS subsystem, the hardware development is started by implementing a basic subset of the target functionality, so that software engineers can immediately start working on it.
This breadboard model will accommodate the MCU, the FPGA and the RF IC\@.
Debugging probes will also be available for easy access of critical signal paths.
At this stage signal reception or transmission will be performed using only the capabilities of the RF IC\@.
To keep the design complexity low, no external PAs or LNAs will be available, so testing will be performed in a controlled environment using a cabling and attenuator setup.
GNU Radio based modulators/demodulators and SDR hardware devices will be used to test basic connectivity with the COMMS\@.
Development on this board will provide feedback to the hardware design engineers and spot possible issues early in the development phase and improve them at the next iterations.
As the software for the COMMS subsystem evolves, the hardware design will simulate the RF characteristics of the COMMS using the Qucs open source circuit simulator.
At the time the COMMS subsystem becomes mature enough, a second design phase will start extending its capabilities with PAs and LNAs for the supported frequency bands.
This is expected to be accomplished with several iterations allowing to an agile development and adaptation to the proposed requirements.
With the appropriate LNAs and PAs attached on the subsystem, series of simulations will be conducted to identify if the performance of the board meets the requirements.
The simulation will be done using the gr-leo GNU Radio simulator which will introduce the LEO channel impairments (noise, variable attenuation, doppler shift).

\section{Ground Segment}

For the ground segment, development of the new socket-based control and data interfaces will start on SatNOGS Radio in parallel with development on SatNOGS Network to support handover, proxying and uplink scheduling.
Once the new SatNOGS Radio interfaces stabilize, SatNOGS client development will begin in order to support these interfaces.
Functional and integration testing will take place to ensure compatibility of new features between subcomponents.
Using a test-driven development process, implementation of automated test cases can start in parallel or preferably before development starts.
Development of ground segment will follow Agile principles, using an iterative approach to eventually satisfy all the specified requirements.

\section{Verification}

After completion of at least one iteration which implements a functional interface, integration testing will take place in order to provide fast feedback and detection of possible errors.
Continuous Integration and Automated Testing will be used to reduce the time and effort for running these tests.

\chapter{Updated Risk Assessment}

This section presents an updated risk assessment with possible threats, their probability of occurrence during the development of the project and possible actions to mitigate them.


\section{Not enough resources on selected FPGA}

\textbf{Probability:} High \\
\textbf{Impact:} Medium-Low \\
\textbf{Initial Mitigation Strategy:} On a later phase of the development, there is a high probability that the FPGA resources are not enough to accommodate the required functionality.
In most cases this problem has to do with the available logical cells and not the available IO pins.
In this case, most of the FPGA manufacturers provide models with the same pinout but with more logical cells, in terms of an increased price.
Incorporating an FPGA with the same pinout but more logical cells requires only a few software modifications. \\
\textbf{Updated Mitigation Strategy (PDR):}  The selected FPGA line (ZYNQ-7020) as of Preliminary Design, offers enough resources to accommodate all requirements set for the product.
A preliminary design, integrating the peripherals that will be used and various DSP operations (filtering, resampling, FFT) resulted to a resource utilization below 50\% as \autoref{fig:vivado} shows.
The final bitstream may greatly vary, but this is a good indication about the capabilities of the \acrshort{fpga}.
In addition, some of the less time-critical operations can be offloaded by the on-chip ARM processors.


\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{diagrams/vivado.png}
  \caption{ZYNQ-7020 resource utilization Vivado report}\label{fig:vivado}
\end{figure}

\section{MCU mis-behavior due to EMI}

\textbf{Probability:} Medium \\
\textbf{Impact:} Medium \\
\textbf{Initial Mitigation Strategy:} Due to the small factor and high transmit power, there is a possibility of the MCU to malfunction due to excess EMI\@.
This can be observed especially in the breadboard models during development.
Proper simulations, ground plane planning and shielding should be able to deal with this problem.  \\
\textbf{Updated Mitigation Strategy (PDR):}  Initial placement of components as of Preliminary Design is taking this risk into account.
More simulations are needed towards the Critical Design milestone.

\section{RF components matching}

\textbf{Probability:} High \\
\textbf{Impact:} High \\
\textbf{Initial Mitigation Strategy:} Network impedance matching is one of the most common problem in RF design and can severely affect the performance of both the reception and the transmission.
The small form factor of the proposed COMMS subsystem is also expected to negatively contribute to the matching problem.
To mitigate this problem, most of the RF components required will be selected to be already matched in the target 50$\Omega$ impedance.
This will eliminate ambiguities in the matching network and simplify the overall PCB design. \\
\textbf{Updated Mitigation Strategy (PDR):}  Careful design and selection of components for the RF networks has been employed as evident in the Preliminary Design.
More simulations are needed towards the Critical Design milestone.

\section{RF emissions fail to meet the regulatory requirements}

\textbf{Probability:} Medium \\
\textbf{Impact:} High \\
\textbf{Initial Mitigation Strategy:} At the final stages of the PCB development and during the testing phase, there is a chance that the emissions of the transmitter may not meet the regulatory requirements, causing interference to adjacent channels etc.
This normally occurs after the signal amplification.
To mitigate this risk, excessive simulations will be performed during the development phase and testing at each iteration to ensure compliance.
In addition, hardware integrated components will be preferred, over custom solutions due to their predictable and documented performance characteristics. \\
\textbf{Updated Mitigation Strategy (PDR):}  Careful design and selection of components for the RF networks has been employed as evident in the Preliminary Design.
More simulations are needed towards the Critical Design milestone.

\section{Software integration issues}

\textbf{Probability:} High \\
\textbf{Impact:} High \\
\textbf{Initial Mitigation Strategy:} A major problem during software development of new systems, is the final integration into a single entity.
Due to the size and the complexity of the proposed COMMS subsystem, final software integration may reveal software bugs.
To avoid them quality assurance and coverage tests will be employed in every software component, throughout the entire software development process. \\
\textbf{Updated Mitigation Strategy (PDR):}  No updates for the Preliminary Design milestone.

\section{Doppler offset exceeds channel capabilities of the RF IC}

\textbf{Probability:} High \\
\textbf{Impact:} Low \\
\textbf{Initial Mitigation Strategy:} Frequency drift due to the doppler effect, especially in LEO channels, varies from few kHz up to 60 kHz in case of the S-band.
This can be an issue for RF ICs and the overall receiver design.
A possible solution could be to increase the filter bandwidth of the receiver, but this of course would have a negative impact to the performance and sensitivity.
We are planning to mitigate this issue with the help of the ground segment.
The ground station prior the transmission will calculate the relative velocity delta, and alter the transmission frequency properly, so the observed frequency at the COMMS receiver after the doppler shift, to lie inside the passband of the filter bandwidth. \\
\textbf{Updated Mitigation Strategy (PDR):} No updates for the Preliminary Design milestone.

\section{Network latency issues in handover proxy mode }

\textbf{Probability:} Medium \\
\textbf{Impact:} Low \\
\textbf{Initial Mitigation Strategy:} During C\&C, network latency can become an issue.
This situation would be more intense on handover proxy mode via SatNOGS Network in which data are relayed between stations.
Latency manifests itself as slow response on commands while in extreme situations it can even render the whole C\&C session useless.
The mitigation action for this issue is to forbid handover sessions in proxy mode for high data rate communications and bands like the S-Band.
Low data rate UHF communication can still utilize the proxy mode and benefit on the simpler network connection configuration it requires. \\
\textbf{Updated Mitigation Strategy (PDR):} No updates for the Preliminary Design milestone.

% \bibliographystyle{unsrt}
% \bibliography{references}

\end{document}
