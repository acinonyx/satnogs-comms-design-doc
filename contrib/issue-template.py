#!/usr/bin/env python3
#
# Script to template out GitLab issues
#
# Copyright (C) 2021 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import re
from pathlib import Path

import gitlab
from jinja2 import Environment, FileSystemLoader

# Create argument parser
parser = argparse.ArgumentParser(description="Template out GitLab issues")
parser.add_argument('--project',
                    metavar='PROJECT',
                    type=str,
                    required=True,
                    help='Project ID or URL-encoded path of the project')
parser.add_argument('--label',
                    metavar='LABEL',
                    action='append',
                    type=str,
                    required=True,
                    help='Filter by label (can be specified multiple times)')
parser.add_argument('template',
                    metavar='PATH',
                    type=str,
                    help='Template source file or directory')

# Parse arguments
args = parser.parse_args()

# Create GitLab client
gl_client = gitlab.Gitlab.from_config()

# Get project
project = gl_client.projects.get(args.project, lazy=True)

# Get issues with labels
issues = project.issues.list(labels=args.label,
                             as_list=False,
                             state='opened',
                             order_by='created_at',
                             sort='asc')

# Create parsed issues list
items = []
for issue in issues:
    # Parse issue
    desc = re.findall(r'^\*{2}(.*?)\*{2}\n+(.*?)\n*(?=\n\*{2}|\Z)',
                      issue.description, re.MULTILINE | re.DOTALL)
    # Create issue item
    item = {
        'id': issue.iid,
        'title': issue.title,
        'labels': issue.labels,
        'content': dict(desc),
    }

    # Append to items list
    items.append(item)

template_path = Path(args.template)

if template_path.is_dir():
    # List all Jinja2 templates
    template_path_list = list(template_path.glob('**/*.j2'))

    for template_path_item in template_path_list:
        # Create Jinja2 environment
        j2_env = Environment(loader=FileSystemLoader(
            '/' if template_path_item.is_absolute() else ''))
        # Load Jinja2 template
        template = j2_env.get_template(str(template_path_item))

        # Write rendered template
        template.stream(issues=items).dump(
            str(template_path_item.relative_to(template_path).with_suffix('')))

else:
    # Create Jinja2 environment
    j2_env = Environment(loader=FileSystemLoader(
        '/' if template_path_item.is_absolute() else ''))

    # Load Jinja2 template
    template = j2_env.get_template(str(template_path))

    # Render template
    print(template.render(issues=items))
