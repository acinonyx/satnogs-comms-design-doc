% SatNOGS COMMS System Requirements Specification
% Copyright (C) 2021 Libre Space Foundation
%
% This work is licensed under a
% Creative Commons Attribution-ShareAlike 4.0 International License.
%
% You should have received a copy of the license along with this
% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

% Linter settings
% chktex-file -2

\documentclass[english,title,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage[toc,acronyms,section,nopostdot,nonumberlist,section=section,numberedsection]{glossaries}
\usepackage[toc,page]{appendix}
\usepackage{graphicx}
\usepackage{titlesec}
\usepackage{booktabs}
\usepackage[hashEnumerators,pipeTables,tableCaptions]{markdown}
\usepackage[maxfloats=500]{morefloats}

\maxdeadcycles=1000
\markdownSetup{
  renderers = {
    ampersand = \&,
    backslash = \textbackslash,
    circumflex = \textasciicircum,
    dollarSign = \$,
    hash = \#,
    leftBrace = \{,
    percentSign = \%,
    pipe = \textpipe,
    rightBrace = \},
    tilde = \textasciitilde,
    underscore = \_,
  },
}

\input{include/version}
\input{include/requirements}

\makeglossaries{}

%% Acronyms
\input{glossaries/acronyms}

\title{System Requirements Specification \\\large{SatNOGS COMMS}}
\author{Libre Space Foundation}
\date{\today\\Version \version\\\textit{DRAFT}}

\titleformat{\chapter}[hang]
{\normalfont\bfseries\Huge}{\thechapter.}{10pt}{}

\begin{document}

\hypersetup{pageanchor=false}
\maketitle
\hypersetup{pageanchor=true}
\tableofcontents


\printglossary[type=\acronymtype]

\chapter{Introduction}

\section{Purpose}

The purpose of this document is to identify and create a complete System Requirements Specifications for implementing SatNOGS COMMS.\@
The analysis shall contain a high level overview of the system components and functions, as well as a complete list of functional and non-functional requirements.

\section{Conventions}

The key words `MUST', `MUST NOT', `REQUIRED', `SHALL', `SHALL NOT', `SHOULD', `SHOULD NOT', `RECOMMENDED', `MAY', and `OPTIONAL' in this document are to be interpreted as described in RFC2119.
However, for readability, these words do not appear in all upper case letters in this specification.

\section{Intended Audience}

The intended audiences of this document are developers who will implement the SatNOGS COMMS solution as well as the European Space Agency and the entities that have signed the Letter of Interest for this development (the latter on an informational capacity).
It is assumed that the reader is familiar with satellite communications in general and more specifically, satellite communication subsystems, both for space and ground segments.
It is also assumed that the reader has some knowledge of radio communications technologies, as well as modulation and decoding schemes and basic knowledge of electronics design.

\section{Scope}

The scope of this SRS is the development of SatNOGS COMMS.\@
SatNOGS COMMS is a versatile telecommunications solution suitable for nano-satellites and Cubesats, operating in UHF and S-band, and featuring tight integration with SatNOGS network.
The scope spans from the space segment transceiver to the ground segment station.

\newpage
\printglossary[type=\acronymtype]

\chapter{Description}

\section{System perspective}

Figure 2 shows a high level overview of SatNOGS COMMS components.
The solution spans both on space and ground segments.
On the space segment, a satellite communications subsystem is specified.
This subsystem is the SatNOGS COMMS transceiver, also referred as  “COMMS transceiver” for simplicity in the rest of the document.
COMMS transceiver consists of radio communications hardware, accompanied by related RF frontend, two communication interfaces for exchanging data with the rest of the satellite subsystems, and a controller.
In addition, a DSP unit is available for additional bulk data processing and cognitive radio functions.
On the ground segment, SatNOGS is used as the solution for management, TC\&C and telemetry data warehouse.
SatNOGS consists of a network of ground stations, a service which coordinates the stations (SatNOGS Network) and databases for storing raw and processed telemetry data.
The operator’s interface is a set of dashboards which gives users access to data warehouse where all telemetry is stored as well as a mission control center allowing operators to telecommand and control the spacecraft.

Figure 2: SatNOGS COMMS Components

\section{System functions}

The high-level function of this system is providing a turnkey radio communications solution for spacecrafts which is tightly integrated with SatNOGS.\@
On the space segment, SatNOGS COMMS transceiver is specified to be capable of concurrent operation on two bands; UHF and S-band.
The transceiver primary functions are modulation and coding, based on CCSDS standard.
This includes QPSK, BPSK, MSK and FSK modulating and demodulating functions.
An I/Q interface is also specified for COMMS transceiver.
This interface is used in conjunction with Cognitive Radio capabilities which are included as secondary functions of COMMS transceiver.
Antenna deploying function is also included as a secondary function.
On the ground segment, SatNOGS COMMS ground station is also specified to be capable to receive and transmit on the respective bands, modulations and coding schemes that the COMMS transceiver supports.
In addition to that, SatNOGS COMMS ground station is fully integrated to SatNOGS, including TC\&C functions.
SatNOGS provides the functions for mission control and telemetry storing and viewing through a set of dashboards which can be configured per customer and/or mission.

\chapter{Requirements}

\section{Functional Requirements}

Functional requirements define the functions that the SatNOGS COMMS solution shall provide in order to meet the requirements of the satellite operator and a Cubesat mission. This section provides the functional requirements for both the ground and the space segment.

\InputIfFileExists{include/requirements-functional}{}{}
\clearpage

\section{Interface Requirements}

This section contains requirements which are related to the interconnection or relationship characteristics between the product and other items.

\InputIfFileExists{include/requirements-interface}{}{}
\clearpage

\section{Environmental Requirements}

This section contains requirements which are related to a product or the system environment during its life cycle.

\InputIfFileExists{include/requirements-environmental}{}{}
\clearpage

\section{Operational Requirements}

This section contains requirements which are related to the system operability.

\InputIfFileExists{include/requirements-operational}{}{}
\clearpage

\section{Logistics Support Requirements}

This section contains requirements related to logistics support considerations to ensure the effective and economical support of a system for its life cycle.

\InputIfFileExists{include/requirements-logistics-support}{}{}
\clearpage

\section{Product Assurance Requirements}

This section contains requirements which are related to the relevant activities covered by the product assurance in order to ensure the quality of the product.

\InputIfFileExists{include/requirements-product-assurance}{}{}
\clearpage

\section{Configuration Requirements}

This section contains requirements which are related to the composition of the product or its organization.

\InputIfFileExists{include/requirements-configuration}{}{}
\clearpage

\section{Design Requirements}

This section contains requirements which are related to the imposed design and construction standards such as design standards, selection list of components or materials, interchangeability, safety or margins.

\InputIfFileExists{include/requirements-design}{}{}
\clearpage

\section{Verification Requirements}

This section contains requirements which are related to the imposed verification methods, such as compliance to verification standards, usage of test methods or facilities.

\InputIfFileExists{include/requirements-verification}{}{}
\clearpage

\section{Performance Requirements}

This section contains requirements which define the minimum acceptable performance characteristics that the system shall fulfill under all possible nominal operation scenarios, providing a predefined and predictable operation.

\InputIfFileExists{include/requirements-performance}{}{}
\clearpage

\end{document}
